-module(afile_server).
-export([start/1, loop/1]).

start(Dir) -> spawn(afile_server, loop, [Dir]). % Create a new Erlang process
                                                % With the module, the function and the list of args

loop(Dir) ->
  receive
    % Pattern match on messages received
    {Client, list_dir} ->
      Client ! {self(), file:list_dir(Dir)};
    {Client, {get_file, File}} ->
      Full = filename:join(Dir,File),
      Client ! {self(), file:read_file(Full)};
    {Client, {read_file, File}} ->
      {ok, Content} = file:read_file(File),
      Client ! {self(), Content};
    {Client, {delete, File}} ->
      file:delete(File)
  end,
  loop(Dir). % Tail call to never end the process of waiting for a message
